<?php

namespace MiniBlog\MiniBlogBundle\Controller;

use MiniBlog\MiniBlogBundle\Entity\Articulo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MiniBlog\MiniBlogBundle\Entity\Comentario;
use MiniBlog\MiniBlogBundle\Form\Frontend\ComentarioType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function blogAction()            
    {
        //Se obtiene un array bidimensional con la lista de artículos y el número de comentarios de cada uno.
        $articuloManager = $this->get('miniblog.manager.articulo');
        $articulos = $articuloManager->findArticulosAndNComentarios();

        return $this->render('MiniBlogBundle:Default:blog.html.twig', array('articulos' => $articulos));
    }
  
    /* Pasar Articulo como parámetro con ParamConverter
     *@ParamConverter ("articulo", class="miniBlogBundle:Articulo")
     */
    public function vistaArticuloAction(Articulo $articulo)
    {

        //Obteniendo lista de comentarios a través del servicio ComentarioManager declarado en el config.yml
        $comentarioManager = $this->get('miniblog.manager.comentario');
        $comentarios = $comentarioManager->findByArticulo($articulo->getId());

        $comentario = new Comentario();
        $comentario->setArticulo($articulo);

        $form = $this->createForm(new ComentarioType(), $comentario);
          
        return $this->render('MiniBlogBundle:Default:vista-articulo.html.twig', array(
            'articulo' => $articulo,
            'comentarios' => $comentarios,
            'form' => $form->createView(),
            ));
    }
    
    /* Pasar Articulo como parámetro con ParameterConverter
     *@ParamConverter ("articulo", class="miniBlogBundle:Articulo")
    */
    public function crearComentarioAction(Articulo $articulo, Request $request)//Obtenida Request como argumento
    {
        $comentario = new Comentario();
        $comentario->setArticulo($articulo);

        $form = $this->createForm(new ComentarioType(), $comentario);
        $form->handleRequest($request);
        
        if ($form->isValid()) {

            //Insertando comentario a través del servicio ComentarioManager declarado en el config.yml
            $comentarioManager = $this->get('miniblog.manager.comentario');
            $comentarioManager->insertComentario($comentario);
        }
        
        return $this->redirect($this->generateUrl('vista_articulo', array('id' => $articulo->getId())));
    }
}
