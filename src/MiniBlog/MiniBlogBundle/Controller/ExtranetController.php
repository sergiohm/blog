<?php

namespace MiniBlog\MiniBlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MiniBlog\MiniBlogBundle\Entity\Articulo;
use MiniBlog\MiniBlogBundle\Entity\Comentario;
use MiniBlog\MiniBlogBundle\Form\Extranet\ArticuloType;
use MiniBlog\MiniBlogBundle\Form\Frontend\ComentarioType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of ExtranetController
 *
 * @author sergio
 */
class ExtranetController extends Controller
{
    public function blogAction()
    {
        //Se obtiene un array bidimensional con la lista de artículos y el número de comentarios de cada uno.
        $articuloManager = $this->get('miniblog.manager.articulo');
        $articulos = $articuloManager->findArticulosAndNComentarios();

        return $this->render('MiniBlogBundle:Extranet:blog.html.twig', array('articulos' => $articulos));
    }

    /* Pasar Articulo como parámetro con ParamConverter
     *@ParamConverter ("articulo", class="miniBlogBundle:Articulo")
     */
    public function vistaArticuloAction(Articulo $articulo)
    {
        //Obteniendo lista de comentarios a través del servicio ComentarioManager declarado en el config.yml
        $comentarioManager = $this->get('miniblog.manager.comentario');
        $comentarios = $comentarioManager->findByArticulo($articulo->getId());

        $comentario = new Comentario();
        $comentario->setArticulo($articulo);

        //Se le pasan los parámetros del action del formulario en un array para no tener que construirlo en la vista
        $form = $this->createForm(new ComentarioType(), $comentario, array(
            'action' => $this->generateUrl('crear_comentario_extranet', array('id' => $articulo->getId()))));

        return $this->render('MiniBlogBundle:Extranet:vista-articulo.html.twig', array(
            'articulo' => $articulo,
            'comentarios' => $comentarios,
            'form' => $form->createView(),
        ));

    }

    /* Pasar Articulo como parámetro con ParameterConverter
     *@ParamConverter ("articulo", class="miniBlogBundle:Articulo")
    */
    public function crearComentarioAction(Articulo $articulo, Request $request)//Obtenida Request como argumento
    {
        $comentario = new Comentario();
        $comentario->setArticulo($articulo);

        $form = $this->createForm(new ComentarioType(), $comentario);
        $form->handleRequest($request);

        if ($form->isValid()) {

            //Insertando comentario a través del servicio ComentarioManager declarado en el config.yml
            $comentarioManager = $this->get('miniblog.manager.comentario');
            $comentarioManager->insertComentario($comentario);
        }

        return $this->redirect($this->generateUrl('vista_articulo_extranet', array('id' => $articulo->getId())));
    }

    public function crearArticuloAction(Request $request)
    {
        $articulo = new Articulo();

        //Se le pasan los parámetros del action del formulario en un array para no tener que construirlo en la vista
        $form = $this->createForm(new ArticuloType(), $articulo, array(
            'action' => $this->generateUrl('crear_articulo')));

        $form->handleRequest($request);

        if($form->isValid())
        {
            $articuloManager = $this->get('miniblog.manager.articulo');
            $articuloManager->insertArticulo($articulo);

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Artículo creado correctamente.'
            );

            return $this->redirect($this->generateUrl('blog_extranet'));
        }

        return $this->render('MiniBlogBundle:Extranet:crear-articulo.html.twig', array(
            'articulo' => $articulo,
            'form_articulo' => $form->createView(),
        ));
    }

    /* Pasar Articulo como parámetro con ParamConverter
     *@ParamConverter ("articulo", class="miniBlogBundle:Articulo")
     */
    public function editarArticuloAction(Articulo $articulo)
    {
        //Obteniendo lista de comentarios a través del servicio ComentarioManager declarado en el config.yml
        $comentarioManager = $this->get('miniblog.manager.comentario');
        $comentarios = $comentarioManager->findByArticulo($articulo->getId());

        $comentario = new Comentario();
        $comentario->setArticulo($articulo);

        //Se le pasan los parámetros del action del formulario en un array para no tener que construirlo en la vista
        $form = $this->createForm(new ArticuloType(), $articulo, array(
            'action' => $this->generateUrl('update_articulo', array('id' => $articulo->getId()))));

        return $this->render('MiniBlogBundle:Extranet:editar-articulo.html.twig', array(
            'articulo' => $articulo,
            'comentarios' => $comentarios,
            'form_articulo' => $form->createView(),
        ));


    }

    /* Pasar Articulo como parámetro con ParameterConverter
     *@ParamConverter ("articulo", class="miniBlogBundle:Articulo")
    */
    public function updateArticuloAction(Articulo $articulo, Request $request)
    {

        $form = $this->createForm(new ArticuloType(), $articulo);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $articuloManager = $this->get('miniblog.manager.articulo');
            $articuloManager->updateArticulo($articulo);
            $this->get('session')->getFlashBag()->add(
                'notice',
                'Artículo editado correctamente.'
            );
        }

        return $this->redirect($this->generateUrl('vista_articulo_extranet', array('id' => $articulo->getId())));
    }

    /* Pasar Articulo como parámetro con ParamConverter
     *@ParamConverter ("articulo", class="miniBlogBundle:Articulo")
     */
    public function eliminarArticuloAction(Articulo $articulo)
    {
        //Obteniendo lista de comentarios a través del servicio ComentarioManager declarado en el config.yml
        $comentarioManager = $this->get('miniblog.manager.comentario');
        $comentarios = $comentarioManager->findByArticulo($articulo->getId());

        return $this->render('MiniBlogBundle:Extranet:eliminar-articulo.html.twig', array(
            'articulo' => $articulo,
            'comentarios' => $comentarios
        ));
    }

    /* Pasar Articulo como parámetro con ParamConverter
     *@ParamConverter ("articulo", class="miniBlogBundle:Articulo")
     */
    public function removeArticuloAction(Articulo $articulo)
    {
        $comentarioManager = $this->get('miniblog.manager.articulo');
        $comentarioManager->removeArticulo($articulo);
        $this->get('session')->getFlashBag()->add(
            'notice',
            'El artículo y sus comentarios han sido borrados correctamente.'
        );

        return $this->redirect($this->generateUrl('blog_extranet'));

    }
}
