<?php

/**
 * Clase para definir formulario para la introducción de comentarios desde la vista frontend.
 *
 * @author sergio
 */

namespace MiniBlog\MiniBlogBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ComentarioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('nombre')
                ->add('email')
                ->add('contenido')
                ->add('Enviar', 'submit')
                ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MiniBlog\MiniBlogBundle\Entity\Comentario'
        ));
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return 'miniblog_miniblogbundle_comentario';
    }
}
