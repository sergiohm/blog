<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Esta clase guarda herramientas útiles que serán usadas en diferentes partes del programa
 *
 * @author sergio
 */

namespace MiniBlog\MiniBlogBundle\Util;

use MiniBlog\MiniBlogBundle\Entity\Comentario;
use MiniBlog\MiniBlogBundle\Form\Frontend\ComentarioType;

class Util {
    
    //Nomraliza una cadena de texto por un valor apto para usarlo como URL
    static public function getSlug($cadena, $separador = '-')
    {
        // Código copiado de http://cubiq.org/the-perfect-php-clean-url-generator
        $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $cadena);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = strtolower(trim($slug, $separador));
        $slug = preg_replace("/[\/_|+ -]+/", $separador, $slug);
        return $slug;
    }
}
