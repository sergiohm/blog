<?php

namespace MiniBlog\MiniBlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Comentario
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MiniBlog\MiniBlogBundle\Entity\ComentarioRepository")
 */

class Comentario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name = "nombre", type="string", length=10)
     * @Assert\NotBlank()
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text")
     * @Assert\NotBlank()
     */
    private $contenido;

    /**
     * @var \DateTime
     * @Assert\Type("\DateTime")
     * @ORM\Column(name="fecha_creacion", type="datetime")
     */
    private $fechaCreacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="modificado", type="boolean")
     */
    private $modificado;

    /**
     * @ORM\ManyToOne(targetEntity="MiniBlog\MiniBlogBundle\Entity\Articulo", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="articulo_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $articulo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Comentario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Comentario
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     * @return Comentario
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;
    
        return $this;
    }

    /**
     * Get contenido
     *
     * @return string 
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Comentario
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    
        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set modificado
     *
     * @param boolean $modificado
     * @return Comentario
     */
    public function setModificado($modificado)
    {
        $this->modificado = $modificado;
    
        return $this;
    }

    /**
     * Get modificado
     *
     * @return boolean 
     */
    public function getModificado()
    {
        return $this->modificado;
    }

    /**
     * Set articulo
     * @param \MiniBlog\MiniBlogBundle\Entity\Articulo $articulo
     * @return Articulo
     */
    public function setArticulo(\MiniBlog\MiniBlogBundle\Entity\Articulo $articulo)
    {
        $this->articulo = $articulo;
    
        return $this;
    }

    /**
     * Get articulo
     *
     * @return \MiniBlog\MiniBlogBundle\Entity\Articulo
     */
    public function getArticulo()
    {
        return $this->articulo;
    }
}