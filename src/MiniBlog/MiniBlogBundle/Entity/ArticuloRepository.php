<?php

namespace MiniBlog\MiniBlogBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Funciones que complementan el Respositorio de la entidad Articulo
 *
 * @author sergio
 */

class ArticuloRepository extends EntityRepository
{

    public function findArticulosAndNComentarios(){

        $em = $this->getEntityManager();

        $dql = 'SELECT a AS articulo, COUNT(c.id) AS nComentarios
                FROM MiniBlogBundle:Articulo a
                LEFT JOIN MiniBlogBundle:Comentario c
                WHERE c.articulo = a.id
                GROUP BY a
                ORDER BY a.fechaCreacion';

        $query = $em->createQuery($dql);

        return $query->getResult();
    }

    public function findComentarios($articulo_id)
    {
        $em = $this->getEntityManager();

        $dql = 'SELECT c
                FROM MiniBlogBundle:Comentario c
                WHERE c.articulo = :id';

        $query = $em->createQuery($dql);
        $query->setParameter('id', $articulo_id);

        return $query->getResult();
    }
}
