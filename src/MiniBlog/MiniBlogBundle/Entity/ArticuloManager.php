<?php
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 20/02/14
 * Time: 9:46
 * */

namespace MiniBlog\MiniBlogBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\Tests\DBAL\Functional\DataAccessTest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class ArticuloManager {

    protected $entityManager;
    protected $class;
    protected $repository;

    /*
     * El contructor recibe los argumentos definidos en el archivo services.yml
     */
    public function __construct(EntityManager $em, $class){

        $this->entityManager = $em;
        $this->repository = $em->getRepository($class);

        $metadata = $em->getClassMetadata($class);
        $this->class = $metadata->getName();

    }

    /*
     * Devuelve un array con dos arrays dentro, uno con la lista de artículos y otro con el número de comentarios
     * de cada artículo, relacionados por el valor del índice del array
     */
    public function findArticulosAndNComentarios(){

        return $this->repository->findArticulosAndNComentarios();
    }

    public function insertArticulo(Articulo $articulo)
    {
        if (isset($articulo))
        {
            $articulo->setFechaCreacion(new \DateTime('now'));

            $this->entityManager->persist($articulo);
            $this->entityManager->flush();
        }
    }

    public function updateArticulo(Articulo $articulo)
    {

        if (isset($articulo))
        {
            $articulo->setFechaModificacion(new \DateTime('now'));

            $this->entityManager->persist($articulo);
            $this->entityManager->flush();
        }

    }

    public function removeArticulo(Articulo $articulo)
    {

        if (isset($articulo))
        {
            $comentarios = $this->repository->findComentarios($articulo->getId());

            if (isset($comentarios))
            {
                foreach ($comentarios as $comentario) {
                 $this->entityManager->remove($comentario);
                }
            }
            $this->entityManager->remove($articulo);
            $this->entityManager->flush();
        }

    }

}