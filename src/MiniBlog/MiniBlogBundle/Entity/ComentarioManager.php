<?php
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 17/02/14
 * Time: 12:18
 *
 * Esta clase se ha definido como servicio en services.yml. Se utiliza para gestionar las acciones referidas
 * a los objetos comentarios en el controlador.
 */

namespace MiniBlog\MiniBlogBundle\Entity;

use Doctrine\ORM\EntityManager;


class ComentarioManager {

    protected $entityManager;
    protected $class;
    protected $repository;

    /*
     * El contructor recibe los argumentos definidos en el archivo services.yml
     */
    public function __construct(EntityManager $em, $class){

        $this->entityManager = $em;
        $this->repository = $em->getRepository($class);

        $metadata = $em->getClassMetadata($class);
        $this->class = $metadata->getName();

    }

    /*
     * Inserta un comentario en la base de datos, usando el EntityManager obtenido en el atributo $entityManager
     */
    public function insertComentario(Comentario $comentario){

        $comentario->setFechaCreacion(new \DateTime('now'));
        $comentario->setModificado(false);
        $this->entityManager->persist($comentario);
        $this->entityManager->flush();
    }

    /*
     * Devuelve una lista de comentarios referidos a un artículo por su id.
     */
    public function findByArticulo($id){

        return $this->repository->findBy(array('articulo' => $id));
    }
} 